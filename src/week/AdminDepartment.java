package week;

public class AdminDepartment extends SuperDepartment{
	
	public String departmentName() {
		
		return "Admin Department";
	}
	
	public String work() {
		
		return "Complete your documents Submission";
	}
	
	public String deadline() {
		
		return "Complete by EOD";
	}
	
}
