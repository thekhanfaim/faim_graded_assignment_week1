package week;

public class SuperDepartment {
	
	public String departmentName() {
		
		return "Super Department";
	}
	
	public String work() {
		
		return "No work as of now";
	}
	
	public String deadline() {
		
		return "Nil";
	}
	
	public String holiday() {
		
		return "Today is not a holiday";
	}
}
