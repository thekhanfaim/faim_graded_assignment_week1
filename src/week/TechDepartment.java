package week;

public class TechDepartment extends SuperDepartment {
	
	public String departmentName() {
		
		return "Tech Department";
	}
	
	public String work() {
		
		return "Complete coding of module 1";
	}
	
	public String deadline() {
		
		return "Complete by EOD";
	}
	
	public String stackInformation() {
		
		return "Core Java";
	}
}
