package week;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("-------Admin Department-------");
		
		AdminDepartment ad = new AdminDepartment();
		System.out.println(ad.departmentName());
		System.out.println(ad.work());
		System.out.println(ad.deadline());
		
		System.out.println("-------HR Department-------");
		
		HRDepartment hd = new HRDepartment();
		System.out.println(hd.departmentName());
		System.out.println(hd.work());
		System.out.println(hd.deadline());
		System.out.println(hd.activity());
		
		System.out.println("-------Tech Department-------");
		
		TechDepartment td = new TechDepartment();
		System.out.println(td.departmentName());
		System.out.println(td.work());
		System.out.println(td.deadline());
		System.out.println(td.stackInformation());
		
	}
	
}
